﻿using CodeBeautify;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodingDojoRestSharp
{
    public partial class Form1 : Form
    {
        CodingDojoConnexionInfo infoConnexion = new CodingDojoConnexionInfo();

        public Form1()
        {
            InitializeComponent();

            LoadInfo();

            Application.ApplicationExit += Application_ApplicationExit;

            swftbUserEmail.DataBindings.Clear();
            swftbTenant.DataBindings.Clear();
            swftbClientId.DataBindings.Clear();
            swftbClientSecret.DataBindings.Clear();

            swftbUserEmail.DataBindings.Add(new Binding("Text", infoConnexion, nameof(CodingDojoConnexionInfo.UserEmail), false, DataSourceUpdateMode.OnPropertyChanged));
            swftbTenant.DataBindings.Add(new Binding("Text", infoConnexion, nameof(CodingDojoConnexionInfo.Tenant), false, DataSourceUpdateMode.OnPropertyChanged));
            swftbClientId.DataBindings.Add(new Binding("Text", infoConnexion, nameof(CodingDojoConnexionInfo.ClientID), false, DataSourceUpdateMode.OnPropertyChanged));
            swftbClientSecret.DataBindings.Add(new Binding("Text", infoConnexion, nameof(CodingDojoConnexionInfo.ClientSecret), false, DataSourceUpdateMode.OnPropertyChanged));
        }

        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            SaveInfo();
        }

        private void swfbGetMails_Click(object sender, EventArgs e)
        {
            try
            {
                CodingDojoMailClient codingDojoMailClient = new CodingDojoMailClient(swftbUserEmail.Text, swftbTenant.Text, swftbClientId.Text, swftbClientSecret.Text);

                codingDojoMailClient.GetMailFolders().ContinueWith(response =>
                {
                    if (response.Exception != null)
                        HandleException(response.Exception);

                    if(response.Result.ResponseStatus == ResponseStatus.Completed)
                    {
                        var mailResponse = MailResponse.FromJson(response.Result.Content);

                        dataGridView1.DataSource = mailResponse.Value;
                    }
                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void SaveInfo()
        {
            var fileName = Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.ApplicationData), "CodingDojoData.json");

            using (StreamWriter file = File.CreateText(fileName))
            {
                file.Write(JsonSerializer.Serialize(infoConnexion));
            }
        }

        private void LoadInfo()
        {
            var fileName = Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.ApplicationData), "CodingDojoData.json");

            if (File.Exists(fileName))
            {
                string fileContent = File.ReadAllText(fileName);
                infoConnexion = (CodingDojoConnexionInfo)JsonSerializer.Deserialize(fileContent, typeof(CodingDojoConnexionInfo));
            }
        }

        private void HandleException(Exception ex)
        {
            string msg = ex.Message;

            if (ex.InnerException != null)
                msg += " " + ex.InnerException.Message;

            MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
