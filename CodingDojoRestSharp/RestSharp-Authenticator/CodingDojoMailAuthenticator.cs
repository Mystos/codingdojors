﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojoRestSharp
{
    class CodingDojoMailAuthenticator : AuthenticatorBase
    {
        readonly string _baseUrl = "";
        readonly string _client_id = "";
        readonly string _client_secret = "";

        public CodingDojoMailAuthenticator(string tenant, string client_id, string client_secret) : base("")
        {
            this._baseUrl = @"https://login.microsoftonline.com/" + tenant;
            this._client_id = client_id;
            this._client_secret = client_secret;
        }

        protected override async ValueTask<Parameter> GetAuthenticationParameter(string accessToken)
        {
            Token = string.IsNullOrEmpty(Token) ? await GetToken() : Token;
            return new HeaderParameter(KnownHeaders.Authorization, "Bearer " + Token);
        }

        private async Task<string> GetToken()
        {
            try
            {
                var client = new RestClient(_baseUrl);

                var request = new RestRequest("/oauth2/v2.0/token")
                    .AddHeader("Content-Type", "application/x-www-form-urlencoded")
                    .AddParameter("client_id", _client_id)
                    .AddParameter("client_secret", _client_secret)
                    .AddParameter("grant_type", "client_credentials")
                    .AddParameter("scope", "https://graph.microsoft.com/.default");

                var response = await client.PostAsync<TokenResponse>(request);
                return response?.access_token;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

