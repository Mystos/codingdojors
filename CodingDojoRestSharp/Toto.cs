﻿
public class Rootobject
{
    public string odatacontext { get; set; }
    public Value[] value { get; set; }
}

public class Value
{
    public string odataetag { get; set; }
    public string id { get; set; }
    public string subject { get; set; }
    public Sender sender { get; set; }
}

public class Sender
{
    public Emailaddress emailAddress { get; set; }
}

public class Emailaddress
{
    public string name { get; set; }
    public string address { get; set; }
}
