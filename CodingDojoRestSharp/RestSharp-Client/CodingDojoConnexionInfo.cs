﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingDojoRestSharp
{
    [Serializable]
    public class CodingDojoConnexionInfo
    {
        private string userEmail = "";
        private string clientID = "";
        private string clientSecret = "";
        private string tenant = "";

        public string UserEmail { get => userEmail; set { userEmail = value; NotifyPropertyChanged("UserEmail"); } }
        public string ClientID { get => clientID; set { clientID = value; NotifyPropertyChanged("ClientID"); } }
        public string ClientSecret { get => clientSecret; set { clientSecret = value; NotifyPropertyChanged("ClientSecret"); } }
        public string Tenant { get => tenant; set { tenant = value; NotifyPropertyChanged("Tenant"); } }

        public CodingDojoConnexionInfo()
        {

        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        //// This method is called by the Set accessor of each property.  
        //// The CallerMemberName attribute that is applied to the optional propertyName  
        //// parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged(String info)
        {
            PropertyChangedEventHandler property_changed = PropertyChanged;
            if (property_changed != null)
            {
                property_changed(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
