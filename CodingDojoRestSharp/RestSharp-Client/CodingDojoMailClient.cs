﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace CodingDojoRestSharp
{
    class CodingDojoMailClient : RestClient
    {
        string userEmail = "";

        public CodingDojoMailClient(string userEmail, string tenant, string client_id, string client_secret) : base(@"https://graph.microsoft.com/v1.0/")
        {
            this.userEmail = userEmail;
            this.Authenticator = new CodingDojoMailAuthenticator(tenant, client_id, client_secret);
        }

        public async Task<RestResponse> GetGraphMails()
        {
            RestRequest restRequest = new RestRequest("users/" + userEmail + "/messages/");

            return await this.GetAsync(restRequest);
        }

        public async Task<RestResponse> GetMailFolders()
        {
            /// users/{id | userPrincipalName}/mailFolders
            RestRequest request = new RestRequest(String.Format("users/{0}/mailFolders", userEmail));
            return await this.GetAsync(request);
        }
    }
}
