﻿
namespace CodingDojoRestSharp
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.swfbGetMails = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.swftbUserEmail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.swftbClientSecret = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.swftbClientId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.swftbTenant = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // swfbGetMails
            // 
            this.swfbGetMails.Location = new System.Drawing.Point(397, 50);
            this.swfbGetMails.Name = "swfbGetMails";
            this.swfbGetMails.Size = new System.Drawing.Size(75, 23);
            this.swfbGetMails.TabIndex = 1;
            this.swfbGetMails.Text = "GetMails";
            this.swfbGetMails.UseVisualStyleBackColor = true;
            this.swfbGetMails.Click += new System.EventHandler(this.swfbGetMails_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.swftbUserEmail);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.swftbClientSecret);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.swftbClientId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.swftbTenant);
            this.groupBox1.Controls.Add(this.swfbGetMails);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(478, 79);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informations de connexion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "User Email :";
            // 
            // swftbUserEmail
            // 
            this.swftbUserEmail.Location = new System.Drawing.Point(78, 19);
            this.swftbUserEmail.Name = "swftbUserEmail";
            this.swftbUserEmail.Size = new System.Drawing.Size(154, 20);
            this.swftbUserEmail.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(218, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Client Secret :";
            // 
            // swftbClientSecret
            // 
            this.swftbClientSecret.Location = new System.Drawing.Point(297, 52);
            this.swftbClientSecret.Name = "swftbClientSecret";
            this.swftbClientSecret.Size = new System.Drawing.Size(94, 20);
            this.swftbClientSecret.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "ClientID :";
            // 
            // swftbClientId
            // 
            this.swftbClientId.Location = new System.Drawing.Point(62, 52);
            this.swftbClientId.Name = "swftbClientId";
            this.swftbClientId.Size = new System.Drawing.Size(129, 20);
            this.swftbClientId.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(265, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tenant :";
            // 
            // swftbTenant
            // 
            this.swftbTenant.Location = new System.Drawing.Point(318, 19);
            this.swftbTenant.Name = "swftbTenant";
            this.swftbTenant.Size = new System.Drawing.Size(154, 20);
            this.swftbTenant.TabIndex = 2;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 97);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(478, 295);
            this.dataGridView1.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 404);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button swfbGetMails;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox swftbClientSecret;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox swftbClientId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox swftbTenant;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox swftbUserEmail;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

